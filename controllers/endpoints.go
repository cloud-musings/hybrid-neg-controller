/*
Copyright 2021 Google LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

// Endpoint represents a network endpoint (IP address,port pair)
type Endpoint struct {
	Address string `json:"address,omitempty"`
	Port    int64  `json:"port,omitempty"`
}

// EndpointList represents a list of Endpoint objects
type EndpointList struct {
	Items []Endpoint `json:"items"`
}

// Creates a new Endpoint object
func NewEndpoint(address string, port int64) *Endpoint {
	return &Endpoint{
		Address: address,
		Port:    port,
	}
}

// Creates a new EndpointList object
func NewEndpointList() *EndpointList {
	return &EndpointList{}
}

// Helper function to check if Endpoint exists in a slice of Endpoints
func (l *EndpointList) containsEndpoint(e Endpoint) bool {
	for _, item := range l.Items {
		if item.Address == e.Address {
			return true
		}
	}
	return false
}

//
func (l *EndpointList) append(e Endpoint) {
	l.Items = append(l.Items, e)
}
