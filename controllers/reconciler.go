/*
Copyright 2021 Google LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"errors"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/builder"
	"sigs.k8s.io/controller-runtime/pkg/client"
	ctrlutil "sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/event"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	networkingv1 "gitlab.com/cloud-musings/hybrid-neg-controller/api/v1"
)

const (
	// Finalizer key used for handling Hybrid NEG object deletions gracefully
	HybridNEGFinalizerKey = "hybridnetworkendpointgroups.networking.cloud.google.com/finalizer"
)

// HybridNetworkEndpointGroupReconciler reconciles a HybridNetworkEndpointGroup object
type HybridNetworkEndpointGroupReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=networking.cloud.google.com,resources=hybridnetworkendpointgroups,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=networking.cloud.google.com,resources=hybridnetworkendpointgroups/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=networking.cloud.google.com,resources=hybridnetworkendpointgroups/finalizers,verbs=update
//+kubebuilder:rbac:groups=core,resources=endpoints,verbs=get;list;watch

// findEndpointGroupsForEndpoints is used to enqueue reconcile requests for
// valid updates to Endpoints objects
func (r *HybridNetworkEndpointGroupReconciler) findEndpointGroupsForEndpoints(endpoints client.Object) []reconcile.Request {
	namespacedName := types.NamespacedName{
		Namespace: endpoints.GetNamespace(),
		Name:      endpoints.GetName(),
	}
	var requests []reconcile.Request
	var hybridNetworkEndpointGroup networkingv1.HybridNetworkEndpointGroup
	if err := r.Get(context.TODO(), namespacedName, &hybridNetworkEndpointGroup); err != nil {
		return []reconcile.Request{}
	}

	requests = append(requests, reconcile.Request{
		NamespacedName: namespacedName,
	})
	return requests
}

// eventFilterPredicate is used to filter change events to HybridNetworkEndpointGroup
// objects to ensure only desired reconcile requests are being enqueued
func (r *HybridNetworkEndpointGroupReconciler) eventFilterPredicate() predicate.Predicate {
	return predicate.Funcs{
		// We ignore delete events, since we are using Finalizers to gracefully
		// manage object deletions, and don't want double requests to be received on
		// a HybridNetworkEndpointGroup object delete event
		DeleteFunc: func(e event.DeleteEvent) bool {
			return false
		},
	}
}

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the state of the environment closer to the desired state.
func (r *HybridNetworkEndpointGroupReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)

	var neg networkingv1.HybridNetworkEndpointGroup
	if err := r.Get(ctx, req.NamespacedName, &neg); err != nil {
		log.Error(err, "unable to fetch hybridnetworkendpointgroup")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	syncer, err := NewHybridNetworkEndpointGroupSyncer(ctx, req, neg.Spec.Project, neg.Spec.Zone, neg.Spec.Port)
	if err != nil {
		log.Error(err, "failed to create hybrid network endpoint groups syncer")
		return ctrl.Result{}, err
	}

	// examine DeletionTimestamp to determine if object is under deletion
	if neg.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then let's add the finalizer and update the object. This is equivalent
		// to registering our finalizer.
		if !containsString(neg.GetFinalizers(), HybridNEGFinalizerKey) {
			ctrlutil.AddFinalizer(&neg, HybridNEGFinalizerKey)
			if err := r.Update(ctx, &neg); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		// DeletionTimestamp is set; the object is under deletion
		if containsString(neg.GetFinalizers(), HybridNEGFinalizerKey) {
			// our finalizer is present, so let's handle any dependency
			log.Info("deleting network endpoint group in Google Cloud")
			if err := syncer.deleteNetworkEndpointGroup(); err != nil {
				log.Error(err, "failed to delete network endpoint group in Google Cloud")
				// if fail to delete the external dependency here, return with error
				// so that it can be retried
				return ctrl.Result{}, err
			}
			log.Info("deleted network endpoint group in Google Cloud")

			// remove our finalizer from the list and update it.
			ctrlutil.RemoveFinalizer(&neg, HybridNEGFinalizerKey)
			if err := r.Update(ctx, &neg); err != nil {
				return ctrl.Result{}, err
			}
		}
		// Stop reconciliation as the item is being deleted
		return ctrl.Result{}, nil
	}

	// Fetch corresponding Endpoints resource
	var endpoints corev1.Endpoints
	if err := r.Get(ctx, req.NamespacedName, &endpoints); err != nil {
		log.Error(err, "unable to fetch endpoints")
		// we'll ignore not-found errors, since they can't be fixed by an immediate
		// requeue (we'll need to wait for a new notification), and we can get them
		// on deleted requests.
		return ctrl.Result{}, err
	}
	log.Info("successfully fetched endpoints resource")

	// Verify valid port in received subsets
	validSubsets := getValidSubsets(endpoints.Subsets, neg.Spec.Port)
	if validSubsets == nil {
		err := errors.New("no valid subsets found")
		return ctrl.Result{}, err
	}
	log.Info("valid subsets found")

	log.Info("generating network endpoints")
	networkEndpoints := generateEndpointList(validSubsets, neg.Spec.Port)

	log.Info("syncing network endpoints to Google Cloud")
	if err := syncer.syncNetworkEndpoints(networkEndpoints); err != nil {
		log.Error(err, "failed to sync endpoints to Google Cloud")
		return ctrl.Result{}, err
	}
	log.Info("network endpoints synced to Google Cloud")

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *HybridNetworkEndpointGroupReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&networkingv1.HybridNetworkEndpointGroup{}).
		Watches(
			&source.Kind{Type: &corev1.Endpoints{}},
			handler.EnqueueRequestsFromMapFunc(r.findEndpointGroupsForEndpoints),
			builder.WithPredicates(predicate.ResourceVersionChangedPredicate{}),
		).
		WithEventFilter(r.eventFilterPredicate()).
		Complete(r)
}
