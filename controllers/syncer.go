/*
Copyright 2021 Google LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	compute "google.golang.org/api/compute/v1"

	ctrl "sigs.k8s.io/controller-runtime"
	log "sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	// Formatting used to generate the name of a NEG in Google Cloud
	endpointGroupNameFormat = "k8s-%s-%s-%d"

	// Type of Google Cloud NEG; hybrid NEG type is the only suppported type
	networkEndpointType = "NON_GCP_PRIVATE_IP_PORT"
)

// HybridNetworkEndpointGroupSyncer syncs endpoints to Cloud Hybrid connectivity NEGs
type HybridNetworkEndpointGroupSyncer struct {
	Context context.Context
	Client  *compute.Service
	Project string
	Zone    string
	Name    string
	Port    int64
}

// Create a new instance of a HybridNetworkEndpointGroupSyncer
func NewHybridNetworkEndpointGroupSyncer(ctx context.Context, req ctrl.Request, project string, zone string, port int64) (*HybridNetworkEndpointGroupSyncer, error) {
	client, err := compute.NewService(ctx)
	if err != nil {
		return &HybridNetworkEndpointGroupSyncer{}, err
	}

	name := fmt.Sprintf(endpointGroupNameFormat, req.NamespacedName.Namespace, req.NamespacedName.Name, port)

	return &HybridNetworkEndpointGroupSyncer{
		Context: ctx,
		Client:  client,
		Project: project,
		Zone:    zone,
		Name:    name,
		Port:    port,
	}, nil
}

// Checks if the NEG exists in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) networkEndpointGroupExists() bool {
	_, err := compute.NewNetworkEndpointGroupsService(s.Client).Get(s.Project, s.Zone, s.Name).Do()
	if err != nil {
		return false
	}
	return true
}

// Deletes the Network Endpoint Group resource from Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) deleteNetworkEndpointGroup() error {
	log := log.FromContext(s.Context)

	if !s.networkEndpointGroupExists() {
		log.Info("NEG does not exist in Google Cloud; doing nothing!")
		return nil
	}

	op, err := compute.NewNetworkEndpointGroupsService(s.Client).Delete(s.Project, s.Zone, s.Name).Do()
	if err != nil {
		log.Error(err, "operation to delete network endpoint group failed")
		return err
	}

	_, err = compute.NewZoneOperationsService(s.Client).Wait(s.Project, s.Zone, op.Name).Do()
	if err != nil {
		log.Error(err, "operation to create network endpoint group failed")
		return err
	}
	return nil
}

// Creates the Network Endpoint Group resource in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) createNetworkEndpointGroup() error {
	log := log.FromContext(s.Context)

	networkEndpointGroup := &compute.NetworkEndpointGroup{
		Name:                s.Name,
		DefaultPort:         s.Port,
		NetworkEndpointType: networkEndpointType,
	}

	op, err := compute.NewNetworkEndpointGroupsService(s.Client).Insert(s.Project, s.Zone, networkEndpointGroup).Do()
	if err != nil {
		log.Error(err, "operation to create network endpoint group failed")
		return err
	}

	_, err = compute.NewZoneOperationsService(s.Client).Wait(s.Project, s.Zone, op.Name).Do()
	if err != nil {
		log.Error(err, "operation to create network endpoint group failed")
		return err
	}
	return nil
}

// Listing function to get all current endpoints in a NEG in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) listNetworkEndpointGroupEndpoints() (EndpointList, error) {
	log := log.FromContext(s.Context)
	req := &compute.NetworkEndpointGroupsListEndpointsRequest{}

	var l EndpointList

	listOfNetworkEndpoints, err := compute.NewNetworkEndpointGroupsService(s.Client).ListNetworkEndpoints(s.Project, s.Zone, s.Name, req).Do()
	if err != nil {
		log.Error(err, "unable to list network endpoint group's network endpoints")
		return l, err
	}

	for _, e := range listOfNetworkEndpoints.Items {
		l.append(Endpoint{
			Address: e.NetworkEndpoint.IpAddress,
			Port:    e.NetworkEndpoint.Port,
		})
	}
	return l, nil
}

// Attaches a collection of endpoints to a NEG in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) attachEndpoints(endpoints EndpointList) error {
	var networkEndpoints []*compute.NetworkEndpoint

	for _, endpoint := range endpoints.Items {
		networkEndpoints = append(networkEndpoints, &compute.NetworkEndpoint{
			IpAddress: endpoint.Address,
			Port:      endpoint.Port,
		})
	}

	req := &compute.NetworkEndpointGroupsAttachEndpointsRequest{
		NetworkEndpoints: networkEndpoints,
	}
	_, err := compute.NewNetworkEndpointGroupsService(s.Client).AttachNetworkEndpoints(s.Project, s.Zone, s.Name, req).Do()
	if err != nil {
		return err
	}
	return nil
}

// Detaches a collection of endpoints from a NEG in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) detachEndpoints(endpoints EndpointList) error {
	var networkEndpoints []*compute.NetworkEndpoint
	for _, endpoint := range endpoints.Items {
		networkEndpoints = append(networkEndpoints, &compute.NetworkEndpoint{
			IpAddress: endpoint.Address,
			Port:      endpoint.Port,
		})
	}

	req := &compute.NetworkEndpointGroupsDetachEndpointsRequest{
		NetworkEndpoints: networkEndpoints,
	}
	_, err := compute.NewNetworkEndpointGroupsService(s.Client).DetachNetworkEndpoints(s.Project, s.Zone, s.Name, req).Do()
	if err != nil {
		return err
	}
	return nil
}

// Syncs endpoints (one-way) in the cluster to a NEG in Google Cloud
func (s *HybridNetworkEndpointGroupSyncer) syncNetworkEndpoints(endpoints EndpointList) error {
	log := log.FromContext(s.Context)

	var endpointsToAttach EndpointList
	var endpointsToDetach EndpointList

	if !s.networkEndpointGroupExists() {
		log.Info("creating NEG in Google Cloud")
		if err := s.createNetworkEndpointGroup(); err != nil {
			return err
		}
	}

	cloudEndpoints, err := s.listNetworkEndpointGroupEndpoints()
	if err != nil {
		return err
	}

	for _, endpoint := range endpoints.Items {
		if !cloudEndpoints.containsEndpoint(endpoint) {
			// Given endpoint, from cluster, is not a network endpoint attached to the
			// NEG in Google Cloud; it will be queued to be attached to the NEG
			endpointsToAttach.append(endpoint)
		}
	}

	for _, endpoint := range cloudEndpoints.Items {
		if !endpoints.containsEndpoint(endpoint) {
			// Given endpoint, from NEG in Google Cloud, is not a network endpoint
			// in the cluster; it will be queued to be detached from the NEG
			endpointsToDetach.append(endpoint)
		}
	}

	if len(endpointsToAttach.Items) > 0 {
		log.Info("attaching endpoints to network endpoint group")
		if err := s.attachEndpoints(endpointsToAttach); err != nil {
			return err
		}
	} else {
		log.Info("no endpoints to attach")
	}

	if len(endpointsToAttach.Items) > 0 {
		log.Info("detaching endpoints from network endpoint group")
		if err := s.detachEndpoints(endpointsToDetach); err != nil {
			return err
		}
	} else {
		log.Info("no endpoints to detach")
	}
	return nil
}
