/*
Copyright 2021 Google LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	corev1 "k8s.io/api/core/v1"
)

// Checks if string exists in a slice of strings
func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

// Generates a slice of Endpoints from EndpointSubset resources
func generateEndpointList(subsets []corev1.EndpointSubset, port int64) EndpointList {
	var endpoints EndpointList
	for _, subset := range subsets {
		for _, address := range subset.Addresses {
			endpoints.append(Endpoint{
				Address: address.IP,
				Port:    port,
			})
		}
	}
	return endpoints
}

// Filters a list of EndpointSubset resources, returning only subsets that
// contain the same port as the defined `port`
func getValidSubsets(subsets []corev1.EndpointSubset, port int64) []corev1.EndpointSubset {
	var validSubsets []corev1.EndpointSubset
	for _, subset := range subsets {
		for _, subsetPort := range subset.Ports {
			if int64(subsetPort.Port) == port {
				validSubsets = append(validSubsets, subset)
			}
		}
	}
	return validSubsets
}
