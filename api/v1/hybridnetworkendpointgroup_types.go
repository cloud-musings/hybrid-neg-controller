/*
Copyright 2021 Google LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// Endpoint defines the singleton Endpoint object
type Endpoint struct {
	Address string `json:"address,omitempty"`
	Port    int64  `json:"port,omitempty"`
}

// HybridNetworkEndpointGroupSpec defines the desired state of HybridNetworkEndpointGroup
type HybridNetworkEndpointGroupSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Google Cloud project ID where this NEG will be created
	Project string `json:"project,omitempty"`

	// Google Cloud compute zone where this NEG will be located
	Zone string `json:"zone,omitempty"`

	// The serving port that will be assigned to endpoints in this endpoints group
	Port int64 `json:"port,omitempty"`
}

// HybridNetworkEndpointGroupStatus defines the observed state of HybridNetworkEndpointGroup
type HybridNetworkEndpointGroupStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// HybridNetworkEndpointGroup is the Schema for the hybridnetworkendpointgroups API
type HybridNetworkEndpointGroup struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   HybridNetworkEndpointGroupSpec   `json:"spec,omitempty"`
	Status HybridNetworkEndpointGroupStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// HybridNetworkEndpointGroupList contains a list of HybridNetworkEndpointGroup
type HybridNetworkEndpointGroupList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []HybridNetworkEndpointGroup `json:"items"`
}

func init() {
	SchemeBuilder.Register(&HybridNetworkEndpointGroup{}, &HybridNetworkEndpointGroupList{})
}
