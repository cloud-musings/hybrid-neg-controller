# Google Cloud Hybrid Network Endpoint Groups Controller

## Description
This repo contains the implementation of a
[Hybrid connectivity Network Endpoint Groups](https://cloud.google.com/load-balancing/docs/negs#hybrid-neg)
controller (`hybrid-neg-controller-manager`). This controller is intended to be
deployed on a Kubernetes cluster that is running off of Google Cloud, but is
["attached"](https://cloud.google.com/anthos/docs/concepts/overview) to a Google
Cloud project.

The Hybrid NEG controller implements the `HybridNetworkEndpointGroup` resource
definition. This resource definition enables you to define
`HybridNetworkEndpointGroup` resources in your cluster, referencing Kubernetes
`Endpoints` (usually generated by `Service` objects). The controller creates a
Hybrid NEG in Google Cloud, corresponding to each `HybridNetworkEndpointGroup`
resource in your cluster. It tracks changes of the corresponding `Endpoints`
resources for each `HybridNetworkEndpointGroup`, and syncs its subsets to the
Google Cloud Hybrid NEG.

The primary use case for this controller is to be used in conjunction with the
Traffic Director service routing API. Users can leverage the Hybrid NEG(s) this
controller manages in Google Cloud to create resources, such as Backend
Services, Routes, Meshes, and Gateways. With this users can deploy a Traffic
Director managed service mesh for workloads running on Kubernetes clusters.

## Getting started
The Hybrid connectivity Network Endpoint Groups controller is deployed just like
any other Kubernetes controller. There are some prerequisites however, in order
for it to operate as intended; the following is a summary of these prerequisites.

* Cluster is [registered with a Google Cloud project](https://cloud.google.com/anthos/multicluster-management/connect/registering-a-cluster#register_clusters_outside)
* Cluster registration is implemented with fleet Workload Identity enabled
* All required Cloud APIs have been enabled in your project

In addition, as an administrator, be sure to have the following permissions in
order to be able to complete the tasks required:
* Enable service APIs
* Create service accounts
* Assign IAM role bindings on the service account resource
* Assign IAM role bindings on the project

### Prerequisites

We start off by creating the required resources in your Google Cloud project.
This consists of creating a Google Cloud service account (GSA), and then
applying a _workload identity user_ IAM role binding on the GSA resource to
enable the Kubernetes service account (KSA) to impersonate the GSA. We then

```shell
# Set env config values
export PROJECT_ID="your-project-id"

# Create a service account
gcloud iam service-accounts create hybrid-neg-controller-manager --project=${PROJECT_ID}

# Assign Workload Identity User IAM role on the GSA to the KSA for impersonation
gcloud iam service-accounts add-iam-policy-binding
  hybrid-neg-controller-manager@${PROJECT_ID}.iam.gserviceaccount.com \
    --role=roles/iam.workloadIdentityUser \
    --member="serviceAccount:${PROJECT_ID}.svc.id.goog[kube-system/hybrid-neg-controller-manager]"

# Assign Kubernetes Service Agent IAM role on the GSA
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
  --role=roles/container.serviceAgent \
  --member="serviceAccount:hybrid-neg-controller-manager@${PROJECT_ID}.iam.gserviceaccount.com"
```

Next, we create a `ConfigMap` that contains the workload identity configuration,
which will be used by each `hybrid-neg-controller-manager` pod to generate and
exchange tokens, so it can make authenticated calls to the Cloud APIs.

```shell
# Set env config values
export PROJECT_ID="your-project-id"
export HUB_MEMBERSHIP="hub-membership-name"

# Generate and apply the ConfigMap
cat <<EOF | kubectl apply -f -
kind: ConfigMap
apiVersion: v1
metadata:
  namespace: kube-system
  name: hybrid-neg-workload-identity-config
data:
  config: |
    {
      "type": "external_account",
      "audience": "identitynamespace:${PROJECT_ID}.svc.id.goog:https://gkehub.googleapis.com/projects/${PROJECT_ID}/locations/global/memberships/${HUB_MEMBERSHIP}",
      "service_account_impersonation_url": "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/hybrid-neg-controller-manager@${PROJECT_ID}.iam.gserviceaccount.com:generateAccessToken",
      "subject_token_type": "urn:ietf:params:oauth:token-type:jwt",
      "token_url": "https://sts.googleapis.com/v1/token",
      "credential_source": {
        "file": "/var/run/secrets/tokens/gcp-ksa/token"
      }
    }
EOF
```

### Deployment

Assuming that all the prerequisites are met, now we will move on to building the
controller's container image and pushing it to a container registry of choice.

```shell
# Set env config values
export IMG="container/image/registry/url:tag" # This is the fully-qualified image url in the registry  

# Build and push container to registry
make docker-build
make docker-push
```

Once the container image is built, and pushed to a container registry that is
accessible by the cluster nodes, you can proceed to install the required
artifacts (mainly the `HybridNetworkEndpointGroup` CRD) and deploy the resources
to your cluster.

```shell
# Set env config values
export IMG="container/registry/url:tag"       # This must be the same as the one set above
export KUBECONFIG="/path/to/kubeconfig/file"  # Only needed if using a custom kubeconfig path

# Install artifacts to target cluster and deploy resources
make install
make deploy
```

Now that all the required resources have been deployed to your cluster, and the
required configurations have been applied to your Google Cloud project, you are
ready to create your first `HybridNetworkEndpointGroup` resource. Select a
`Service` object, for which you would like the controller to create a Hybrid
connectivity Network Endpoint Group, and apply the following configuration.

```shell
# Set env config values
export PROJECT_ID="your-project-id"
export SERVICE_NAME="your-special-service"
export SERVICE_NAMESPACE="your-special-service-namespace"
export CONTAINER_PORT="9443"  # This should be the same as `TargetPort` in your `Service` object
export ZONE="us-central1-a"

# Generate and apply the ConfigMap
cat <<EOF | kubectl apply -f -
apiVersion: networking.cloud.google.com/v1
kind: HybridNetworkEndpointGroup
metadata:
  name: "${SERVICE_NAME}"
  namespace: "${SERVICE_NAMESPACE}"
spec:
  project: "${PROJECT_ID}"
  zone: "${ZONE}"
  port: ${CONTAINER_PORT}
EOF
```

Once your configuration has been applied successfully to the cluster, the
controller will create a Hybrid NEG in the Google Cloud project with the name
format `k8s-{SERVICE_NAMESPACE}-{SERVICE_NAME}-{CONTAINER_PORT}`.
> _TODO: develop a better naming scheme for NEGs, or possibly introduce a field
for the user to define it_

You can describe the NEG, and list its endpoints, using the following commands.
```shell
# Set env config values
export NEG_NAME="k8s-${SERVICE_NAMESPACE}-${SERVICE_NAME}-${CONTAINER_PORT}"

# Describe the Network Endpoint Group
gcloud compute network-endpoint-groups describe ${NEG_NAME} \
  --project=${PROJECT_ID} \
  --zone=${ZONE}

# List the endpoints of the Network Endpoint Group
gcloud compute network-endpoint-groups list-network-endpoints ${NEG_NAME} \
  --project=${PROJECT_ID} \
  --zone=${ZONE}
```

### Cleaning up

If you need to clean up your cluster, removing all resources and artifacts that
were deployed in the previous step, you can do so by running the following
commands.

```shell
# Set env config values
export IMG="container/registry/url:tag"       # This must be the same as the one set above
export KUBECONFIG="/path/to/kubeconfig/file"  # Only needed if using a custom kubeconfig path

# Uninstall artifacts from target cluster and remove resources
make undeploy
make uninstall
```

## Disclaimer

This project is not an officially supported Google Cloud project. It is being
developed (and supported) on a best effort basis by myself, for the time being,
and any interested contributors interested.

### Testing
> TODO

## Authors and acknowledgment
> TODO

## References
> TODO
